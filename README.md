<h1>P1: Mesquite Bookstore</h1>

<h2>Introduction</h2>
In this project, I refactored the backend and built the frontend for my application. First, I refurbished
the means of communication between my server and database using Hibernate object-relational mapping
Java framework, which leverages the Java Persistence API and JDBC. Second, I developed the frontend of
my application using HTML, CSS, and Javascript. In the end, we have a full-stack application.


<h2>Microsoft Azure SQL Database:</h2>
<h3>Initial tables</h3>
<strong>book table:</strong>  ![book.png](./assets/book.png)

<strong>feedback table:</strong> ![feedback.png](./assets/feedback.png)

<strong>bookstore_user table:</strong> ![bookstore_user.png](assets/bookstore_user.png)

<h3>Final tables</h3>
<strong>book table:</strong>

<strong>feedback table:</strong>

<h2>Backend Routes</h2>
<ul>
<li><strong>Backend:</strong> [CRUD] HTTP methods supported</li>
<ul>
<li>GET: /books and / feedback </li>
<li>GET: /books/n </li>

<li>PUT: /books/n </li>

<li>POST: /books and /login </li>

<li>DELETE: /books/n </li></ul>


<h2>Frontend Routes</h2>
<li><strong>Frontend:</strong> HTTP methods supported</li>
<ul>
<li>GET: /books [inventory.html] and /feedback [feedback.html] </li>
<li>POST: /books [add-book.html], /feedback [contact.html], and /login [login.html] </li>
</ul>

</ul>

<h2>Frontend Functionality</h2>
<ul>
<li>home.html: landing page.</li>
<li>about.html: displays information about the vendor.</li>

<li>inventory.html: displays the inventory of books.</li>
<li>feedback.html: displays feedback created in the [contact.html] </li>
<li>add-book.html: adds a record into the Azure database</li>
<li>login.html: provides login functionality for vendor's employees</li>
</ul>

<h2>Basic Login Functionality</h2>
<li>Scenario A:</li>

PASS: Show during the presentation.

<li>Scenario B:</li>

FAIL: Show during the presentation.

<h2>Testing</h2>

<h3>Testing [Via Postman]</h3>
<li>GET Method:	/books</li>

Show during presentation

<li>GET	Method: /books/5</li>

Show during presentation

<li>POST Method: /books</li>

...

<li>DELETE Method: /books/17</li>

...

<li>PUT Method:	/books/1</li>

...


<h3>Unit Testing</h3>

<h4>FAILED</h4>




<h2>Design Patterns:</h2>

<ul>
<li>Model-View-Controller [FRONTEND]</li> 
<li>Singleton [BACKEND]</li> 
</ul>

<h2>Bootstrap templates</h2>
<ul>
    Custom Components
    <ul>
        <li>Cover [Home]: A one-page template for building home pages.</li> 
        <li>Sign-in [Login. Contact Us.]: Custom form layout and design for a simple sign in form.</li>
        <li>Offcanvas navbar [Website]: Reasponsive and expandable navbar.</li>
    </ul>
    Integrations
    <ul>
        <li>Masonry [About Us]: Bootsrap grid and the Masonry layout.</li> 
    </ul>
</ul>


<h2>Recommended Stretch Goals</h2>
<ul>Eternal API
    <ul><li>The Google Maps API [About Us].</li></ul>
</ul>

<ul>Aesthetics
    <ul><li>Integrated bootstrap into my web pages.</li></ul>
<ul>
<li>Integrated User Experience (UX) concepts into my web pages.</li>
<ul>
    <li><strong>Consistency and Standard</strong>: Website.</li>
    <li><strong>Aesthetic and Minimalist Design</strong>: Website.</li>
    <li><strong>Help Users Recognize, Diagnose, and Recover from Errors</strong>: [Contact Us].</li>
    <li><strong>User Centered Design</strong>: Put potential end-user at the helm and shape the frontend from her perspective.</li>
        <ul>
            <li>Interactions. Look and Feel.</li>
            <li>For example: [Home],[About Us], etc..</li>
        </ul>
</ul>
</ul>
</ul>


<h3>Security Concerns:</h3>
<ul>
<li>Problem: Injection. Solution: Hibernate.</li>
<li>Problem: Broken Authentication. Broken Access Control. </li>
</ul>


<h2>Self-Assessment</h2>
<ul>
<li>Again, integrate authentication with JWT.</li>

<li>Integrate more testing into future projects.</li>

<li>Refine frontend.</li>

</ul>


<h2>External Links:</h2>
<ul>
<li>Landing page: https://mrodriguezblobstorage.blob.core.windows.net/mbs-frontend-v1/views/home.html </li>
</ul>


<h2>4//5/2021:</h2>
