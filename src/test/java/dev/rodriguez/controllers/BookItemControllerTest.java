package dev.rodriguez.controllers;

import dev.rodriguez.controlers.BookItemController;
import dev.rodriguez.model.BookItem;
import dev.rodriguez.services.BookItemService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class BookItemControllerTest {

    @InjectMocks
    private BookItemController bookItemController;

    @Mock
    private BookItemService service;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

/**
    @Test
    public void testHandlerGetAllItemsRequest(){
        Context context = mock(Context.class);

        List<BookItem> items = new ArrayList<>();

        items.add(new BookItem(10, "Philosophy For Dummies", 1.00));
        items.add(new BookItem(1000, "Game of Thrones and Philosophy: Logic Cuts Deeper Than Swords", 17.99));
        items.add(new BookItem(100000, "Austin Powers International Man of Mystery", 1000000));


        when(service.getAllBooks()).thenReturn(items);

        bookItemController.handleGetAllItemsRequest(context);
        verify(context.json(items));
    }
*/




}
