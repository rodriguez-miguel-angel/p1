package dev.rodriguez.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="feedback")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Feedback implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="feedback_id")
    private int feedbackID;

    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;

    @Column(name="feedback_msg")
    private String feedbackMSG;

    public Feedback() {
        super();
    }

    public Feedback(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    public Feedback(String firstName, String lastName, String feedbackMSG) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.feedbackMSG = feedbackMSG;
    }

    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFeedbackMSG() {
        return feedbackMSG;
    }

    public void setFeedbackMSG(String feedbackMSG) {
        this.feedbackMSG = feedbackMSG;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Feedback feedback = (Feedback) o;
        return feedbackID == feedback.feedbackID && Objects.equals(firstName, feedback.firstName) && Objects.equals(lastName, feedback.lastName) && Objects.equals(feedbackMSG, feedback.feedbackMSG);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feedbackID, firstName, lastName, feedbackMSG);
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "feedbackID=" + feedbackID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", feedbackMSG='" + feedbackMSG + '\'' +
                '}';
    }
}
