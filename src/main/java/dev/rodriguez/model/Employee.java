package dev.rodriguez.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="employee")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="employee_id")
    private int employeeID;

    private String first_name;
    private String middle_name;
    private String last_name;

    private String employeeIdentifcationNumber;

    private String emailAddress;

    public Employee() {
        super();
    }

    public Employee(int employeeID) {
        this.employeeID = employeeID;
    }

    public Employee(String first_name, String last_name, String employeeIdentifcationNumber) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.employeeIdentifcationNumber = employeeIdentifcationNumber;
    }

    public Employee(String first_name, String last_name, String employeeIdentifcationNumber, String emailAddress) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.employeeIdentifcationNumber = employeeIdentifcationNumber;
        this.emailAddress = emailAddress;
    }

    public Employee(String first_name, String middle_name, String last_name, String employeeIdentifcationNumber, String emailAddress) {
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.employeeIdentifcationNumber = employeeIdentifcationNumber;
        this.emailAddress = emailAddress;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmployeeIdentifcationNumber() {
        return employeeIdentifcationNumber;
    }

    public void setEmployeeIdentifcationNumber(String employeeIdentifcationNumber) {
        this.employeeIdentifcationNumber = employeeIdentifcationNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeID == employee.employeeID && Objects.equals(first_name, employee.first_name) && Objects.equals(middle_name, employee.middle_name) && Objects.equals(last_name, employee.last_name) && Objects.equals(employeeIdentifcationNumber, employee.employeeIdentifcationNumber) && Objects.equals(emailAddress, employee.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first_name, middle_name, last_name, employeeID, employeeIdentifcationNumber, emailAddress);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "first_name='" + first_name + '\'' +
                ", middle_name='" + middle_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", employeeID=" + employeeID +
                ", employeeIdentifcationNumber='" + employeeIdentifcationNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
