package dev.rodriguez.main;

import dev.rodriguez.controlers.AuthController;
import dev.rodriguez.controlers.BookItemController;
import dev.rodriguez.controlers.FeedbackController;
import dev.rodriguez.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;


public class JavalinApp {

    BookItemController bookItemController = new BookItemController();
    FeedbackController feedbackController = new FeedbackController();

    AuthController authController = new AuthController();

    SecurityUtil securityUtil = new SecurityUtil();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{
        path("books", ()->{

            before("/", authController::authorizeToken);
            get(bookItemController::handleGetAllItemsRequest);
            post(bookItemController::handlePostNewBookItemRequest);
            path(":id", ()->{
                before("/", authController::authorizeToken);
                get(bookItemController::handleGetItemByIDRequest);
                put(bookItemController::handlePutUpdatePriceByIDRequest);
                delete(bookItemController::handleDeleteItemByIDRequest);
            });
        });
        path("feedback", ()->{

            before("/", authController::authorizeToken);
            get(feedbackController::handleGetAllFeedbackRequest);
            post(feedbackController::handlePostNewFeedbackItemRequest);
            path(":id", ()->{
                before("/", authController::authorizeToken);
                get(feedbackController::handleGetIndividualFeedbackByIDRequest);
                delete(feedbackController::handleDeleteFeedbackByIDRequest);
            });
        });
        path("login",()-> {
            post(authController::authenticateLogin);
            after("/", securityUtil::attachResponseHeaders);
        });
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.start();
    }

}
