package dev.rodriguez.customtype;

import dev.rodriguez.model.BookCategory;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomBookCategoryType implements UserType {

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (BookCategory) deepCopy(o);
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if(x == y) {
            return true;
        }
        if(x == null || y == null) {
            return false;
        } else {
            return x.equals(y);
        }
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        BookCategory category = (BookCategory) x;
        return category.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        BookCategory category = null;
        String name = rs.getString(names[0]);   // retrieves an instance of the map class from a JDBC resultSet
        if(name != null) {
            // the actual conversion from string to Enum
            category = BookCategory.getBookCategory(name);
        }
        return category;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        if(value == null) {
            st.setNull(index, StringType.INSTANCE.sqlType());   // writes an instance of the map class to a preparedStatement
        } else {
            // the conversion from Enum to String
            BookCategory category = (BookCategory) value;
            st.setString(index, category.name());
        }
    }


    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }

    @Override
    public int[] sqlTypes() {
        return new int[] {StringType.INSTANCE.sqlType()};
    }

    @Override
    public Class returnedClass() {
        return CustomBookCategoryType.class;
    }

}
