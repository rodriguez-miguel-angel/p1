package dev.rodriguez.services;


import dev.rodriguez.data.BookItemDao;
import dev.rodriguez.data.BookItemDaoHibImpl;
import dev.rodriguez.data.BookItemDaoImpl;
import dev.rodriguez.model.BookItem;
import io.javalin.http.BadRequestResponse;

import java.util.List;

/**
 * business logic:
 *
 */


public class BookItemService {
    private final BookItemDao bookItemDao = new BookItemDaoHibImpl();

    // crud method: read
    public List<BookItem> getAllBooks() {
        return bookItemDao.getAllBookItems();
    }

    // crud method: read
    public BookItem getBookByItemID(int itemID) {
        return bookItemDao.getBookItemByItemID(itemID);
    }

    // crud method: create
    public BookItem addBook(BookItem item) {
        return bookItemDao.addNewBookItem(item);
    }

    // crud method: delete
    public void removeBook(int itemID) {
        bookItemDao.deleteBookItem(itemID);
    }

    // crud method: update
    public void updateBookPrice(int itemID, double price) {
        bookItemDao.updateBookItemPrice(itemID, price);
    }

    public List<BookItem> getBooksInRange(String minPrice, String maxPrice) {
        if(minPrice!=null){
            if(matchesPriceFormat(minPrice)) {
                if(maxPrice!=null) {
                    if(matchesPriceFormat(maxPrice)) {
                        return bookItemDao.getBookItemsInRange(Double.parseDouble(minPrice),Double.parseDouble(maxPrice));
                    }
                    throw new BadRequestResponse("Error: improper price format for max-price.");
                }
                /**
                 * min-price scenario:
                 */
                return bookItemDao.getBookItemsWithMinPrice(Double.parseDouble(minPrice));
            }
            throw new BadRequestResponse("Error: improper price format for min-price.");
        } else {
            /**
             * max-price scenario:
             */
            if(matchesPriceFormat(maxPrice)) {
                return bookItemDao.getBookItemsWithMaxPrice(Double.parseDouble(maxPrice));
            }
            throw new BadRequestResponse("Error: improper price format for max-price.");
        }
    }

    /**
     * input validation:
     */
    private boolean matchesPriceFormat(String str) {
        return str.matches("^\\d{0,6}(\\.\\d{1,2})?");
    }

    private boolean matchesEmailFormat(String str) {
        return str.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }

    private boolean matchesUsernameFormat(String str) {
        return str.matches("^[\\w-\\.]+");
    }



}
