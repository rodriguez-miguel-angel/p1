package dev.rodriguez.data;

import dev.rodriguez.model.BookCondition;
import dev.rodriguez.model.BookItem;

import java.util.List;

public interface BookItemDao {

    /**
     * crud method: read
     */
    List<BookItem> getAllBookItems();

    BookItem getBookItemByItemID(int itemID);

    List<BookItem> getBookItemsInRange(double min, double max);
    List<BookItem> getBookItemsWithMaxPrice(double max);
    List<BookItem> getBookItemsWithMinPrice(double min);

    /**
     * crud method: create
     */
    BookItem addNewBookItem(BookItem item);

    /**
     * crud method: delete
     */
    void deleteBookItem(int itemID);

    /**
     * crud method: update
     */
    void updateBookItemPrice(int itemID, double price);

    void updateBookItemCondition(int itemID, BookCondition condition);
}
