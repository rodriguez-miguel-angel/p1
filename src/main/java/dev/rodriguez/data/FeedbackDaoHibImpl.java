package dev.rodriguez.data;


import dev.rodriguez.model.Feedback;
import dev.rodriguez.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * performs various crud operations such as CREATE, READ, UPDATE, and DELETE operations using Hibernate
 */
public class FeedbackDaoHibImpl implements FeedbackDao {

    private final Logger logger = LoggerFactory.getLogger(FeedbackDaoHibImpl.class);
    /**
     * crud method: read
     */
    @Override
    public List<Feedback> getAllFeedback() {
        try (Session s = HibernateUtil.getSession()) {
            /**
             * type safe:
             *
             * note: queryString here is the HQL (hibernate query language) equivalent to "select * from feedback"
             */

            List<Feedback> feedback = s.createQuery("from Feedback", Feedback.class).list();
            return feedback;
        }
    }

    @Override
    public Feedback getIndividualFeedbackByFeedbackID(int feedbackID) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            Feedback individualFeedback = session.get(Feedback.class, feedbackID);
            logger.info("Feedback: " + individualFeedback + ". id:" + feedbackID);
            return individualFeedback;
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    /**
     * crud method: create
     */
    @Override
    public Feedback addFeedback(Feedback feedback) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            int id = (int) session.save(feedback);
            feedback.setFeedbackID(id);
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful insert...");
            session.close();
        }
        return feedback;
    }


    /**
     * crud method: delete
     */
    @Override
    public void deleteFeedback(int feedbackID) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.delete(new Feedback(feedbackID));
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful delete...");
            session.close();
        }
    }

}
