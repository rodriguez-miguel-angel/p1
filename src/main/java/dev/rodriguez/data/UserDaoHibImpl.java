package dev.rodriguez.data;

import dev.rodriguez.model.User;
import dev.rodriguez.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.util.List;

/**
 * performs various crud operations such as CREATE, READ, UPDATE, and DELETE operations using Hibernate
 */
public class UserDaoHibImpl implements UserDao {

    private final Logger logger = LoggerFactory.getLogger(UserDaoHibImpl.class);


    /**
     * crud method: read
     */
    @Override
    public List<User> getAllUsers() {
        try (Session s = HibernateUtil.getSession()) {
            /**
             * type safe:
             *
             * note: queryString here is the HQL (hibernate query language) equivalent to "select * from book"
             */

            List<User> users = s.createQuery("from User", User.class).list();
            return users;
        }
    }

    @Override
    public User getUserByUserID(int userID) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            User user = session.get(User.class, userID);
            logger.info("User: " + user + ". id:" + userID);
            return user;
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;

    }


    /**
     * crud method: create
     */
    @Override
    public User addUser(User user) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            int id = (int) session.save(user);
            user.setUserID(id);
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful insert...");
            session.close();
        }
        return user;
    }


    /**
     * crud method: delete
     */
    @Override
    public void deleteUser(int userID) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.delete(new User(userID));
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful delete...");
            session.close();
        }

    }


    @Override
    public void updateUserPassword(int userID, String password) {

    }

    @Override
    public boolean doesUserExist(String userName) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);
            cq.select(root);

            cq.where(cb.equal(root.get("userName"),userName));
            Query<User> query = session.createQuery(cq);


            if(!query.list().isEmpty()) {
                logger.info("Success: Recognized user");
                return true;
            } else {
                logger.info("Error: Unrecognized user");
                return false;
            }

        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }

        return false;
    }

    @Override
    public User getUserByUserName(String userName) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);
            cq.select(root);

            cq.where(cb.equal(root.get("userName"),userName));
            Query<User> query = session.createQuery(cq);

            query.setMaxResults(1);

            User user = query.getSingleResult();
            return user;
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);
            cq.select(root);

            cq.where(cb.equal(root.get("email"),email));
            Query<User> query = session.createQuery(cq);

            query.setMaxResults(1);

            User user = query.getSingleResult();

            return user;
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }




    @Override
    public String getPasswordByUserName(String userName) {
        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);

            Root<User> root = cq.from(User.class);
            cq.select(root);

            cq.where(cb.equal(root.get("userName"),userName));
            Query<User> query = session.createQuery(cq);

            query.setMaxResults(1);

            User user = query.getSingleResult();
            return user.getPassword();
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }


}
