package dev.rodriguez.data;


import dev.rodriguez.model.BookCondition;
import dev.rodriguez.model.BookItem;
import dev.rodriguez.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


// the connection object establishes connections with a database to execute SQL statements against it
// in addition, perform other operations such as commits and rollbacks

/**
 * Data Access Object (DAO): JDBC used to connect to database
 */


public class BookItemDaoImpl implements BookItemDao {

    private final Logger logger = LoggerFactory.getLogger(BookItemDaoImpl.class);

    /**
     * crud method: read
     */
    @Override
    public List<BookItem> getAllBookItems() {
        List<BookItem> items = new ArrayList<>();

        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try (Connection connection = ConnectionUtil.getConnection();
            Statement statement = connection.createStatement())
        {
            ResultSet resultSet = statement.executeQuery("select * from public.book");

            while(resultSet.next()) {
                int itemID = resultSet.getInt("book_id");
                double price = resultSet.getDouble("price");
                String title = resultSet.getString("title");
                BookItem item = new BookItem(itemID, title, price);
                items.add(item);
            }
            logger.info("The connection has been successfully established!");
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return items;
    }

    @Override
    public BookItem getBookItemByItemID(int itemID) {

        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from public.book where book_id = ?")) {
            preparedStatement.setInt(1,itemID);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                String title = resultSet.getString("title");
                double price = resultSet.getDouble("price");
                logger.info("Book retrieved from database by id: {}", itemID);
                return new BookItem(itemID, title, price);
            }

        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    @Override
    public List<BookItem> getBookItemsInRange(double min, double max) {
        List<BookItem> items = new ArrayList<>();
        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from book where price > ? and < ?")) {
            preparedStatement.setDouble(1, min);
            preparedStatement.setDouble(2, max);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int itemID = resultSet.getInt("book_id");
                String title = resultSet.getString("title");
                double price = resultSet.getDouble("price");
                items.add(new BookItem(itemID, title, price));
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        logger.info("Successful read...");
        return items;
    }

    public List<BookItem> getBookItemsWithMaxPrice(double max) {
        List<BookItem> items = new ArrayList<>();
        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from public.book where price < ?")) {
            preparedStatement.setDouble(1, max);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                int itemID = resultSet.getInt("book_id");
                String title = resultSet.getString("title");
                double price = resultSet.getDouble("price");
                items.add(new BookItem(itemID, title, price));
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        logger.info("Successful read...");
        return items;
    }

    @Override
    public List<BookItem> getBookItemsWithMinPrice(double min) {
        List<BookItem> items = new ArrayList<>();
        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select * from public.book where price > ?")) {
            preparedStatement.setDouble(1, min);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                int itemID = resultSet.getInt("book_id");
                String title = resultSet.getString("title");
                double price = resultSet.getDouble("price");
                items.add(new BookItem(itemID, title, price));
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        logger.info("Successful read...");
        return items;
    }

    /**
     * crud method: create
     */
    @Override
    public BookItem addNewBookItem(BookItem item) {
        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("insert into public.book (title, price) values (?, ?)")) {
            preparedStatement.setString(1, item.getBookTitle());
            preparedStatement.setDouble(2, item.getPrice());
            preparedStatement.executeUpdate();
            logger.info("Successful create...");
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return item;
    }

    /**
     * crud method: delete
     */
    @Override
    public void deleteBookItem(int itemID) {

        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("delete from public.book where book_id = ?")){;
            preparedStatement.setInt(1, itemID);
            preparedStatement.executeUpdate();
            logger.info("Successful delete...");
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
    }

    /**
     * crud method: update
     */
    @Override
    public void updateBookItemPrice(int itemID, double price) {
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("update public.book set price = ? where book_id  = ?")) {
            preparedStatement.setDouble(1, price);
            preparedStatement.setInt(2, itemID);
            preparedStatement.executeUpdate();
            logger.info("Successful update...");
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }

    }

    /**
     * vn:
     * crud method: update
     */
    public void updateBookItemCondition(int itemID, BookCondition condition) {

    }

}
