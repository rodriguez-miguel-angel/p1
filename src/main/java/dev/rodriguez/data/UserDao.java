package dev.rodriguez.data;

import dev.rodriguez.model.User;

import java.util.List;

public interface UserDao {


    /**
     * crud method: read
     */
    List<User> getAllUsers();

    User getUserByUserID(int userID);

    User getUserByUserName(String userName);

    User getUserByEmail(String email);

    /**
     *
     * vn:
     *
     *  User addUser(User user);
     *
     */

    User addUser(User user);

    /**
     * crud method: create
     */


    /**
     * crud method: delete
     */
    void deleteUser(int userID);

    /**
     * crud method: update
     */
    void updateUserPassword(int userID, String password);


    /**
     *
     *  version n:
     */
    boolean doesUserExist(String userName);

    String getPasswordByUserName(String userName);

}
