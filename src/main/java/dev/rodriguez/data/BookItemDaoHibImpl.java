package dev.rodriguez.data;

import dev.rodriguez.model.BookCondition;
import dev.rodriguez.model.BookItem;
import dev.rodriguez.util.HibernateUtil;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


/**
 * performs various crud operations such as CREATE, READ, UPDATE, and DELETE operations using Hibernate
 */

public class BookItemDaoHibImpl implements BookItemDao {

    private final Logger logger = LoggerFactory.getLogger(BookItemDaoHibImpl.class);

    /**
     * crud method: read
     */
    @Override
    public List<BookItem> getAllBookItems() {
        try (Session s = HibernateUtil.getSession()) {
            /**
             * type safe:
             *
             * note: queryString here is the HQL (hibernate query language) equivalent to "select * from book"
             */

            List<BookItem> items = s.createQuery("from BookItem", BookItem.class).list();
            return items;
        }
    }

    @Override
    public BookItem getBookItemByItemID(int itemID) {

        /**
         * Session implements Autocloseable
         *      -> try with resources
         */
        try(Session session= HibernateUtil.getSession()) {
            BookItem item = session.get(BookItem.class, itemID);
            logger.info("Book item: " + item + ". id:" + itemID);
            return item;
        } catch (HibernateException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    /**
     * crud method: create
     */
    @Override
    public BookItem addNewBookItem(BookItem item) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            int id = (int) session.save(item);
            item.setBookID(id);
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful insert...");
            session.close();
        }
        return item;
    }


    /**
     * crud method: delete
     */
    @Override
    public void deleteBookItem(int itemID) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.delete(new BookItem(itemID));
        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful delete...");
            session.close();
        }

    }

    /**
     * crud method: update
     */
    @Override
    public void updateBookItemPrice(int itemID, double price) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<BookItem> cq = cb.createQuery(BookItem.class);

            Root<BookItem> root = cq.from(BookItem.class);
            cq.select(root);

            cq.where(cb.equal(root.get("bookID"),itemID));
            Query<BookItem> query = session.createQuery(cq);

            BookItem book = query.getSingleResult();
            book.setPrice(price);

        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful update...");
            session.close();
        }

    }



    /**
     * crud method: read
     */

    @Override
    public List<BookItem> getBookItemsInRange(double min, double max) {
        return null;
    }

    @Override
    public List<BookItem> getBookItemsWithMaxPrice(double max) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        List<BookItem> items = null;

        try {
            transaction = session.beginTransaction();
            Filter filter = session.enableFilter("maxFilter");
            filter.setParameter("priceParam", max);

            items = session.createQuery("from Book").list();

            session.disableFilter("maxFilter");

        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful insert...");
            session.close();
        }
        return items;
    }

    @Override
    public List<BookItem> getBookItemsWithMinPrice(double min) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        List<BookItem> items = null;

        try {
            transaction = session.beginTransaction();
            Filter filter = session.enableFilter("minFilter");
            filter.setParameter("priceParam", min);

            items = session.createQuery("from Book").list();

            session.disableFilter("minFilter");

        } catch (HibernateException ex) {
            transaction.rollback(); //rollback transaction in order not to leave database in an inconsistent state
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        } finally {
            transaction.commit();
            logger.info("Successful insert...");
            session.close();
        }
        return items;
    }




    /**
     * vn:
     * crud method: update
     */
    public void updateBookItemCondition(int itemID, BookCondition condition) {

    }

}
