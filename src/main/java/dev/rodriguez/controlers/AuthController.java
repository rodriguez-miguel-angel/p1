package dev.rodriguez.controlers;

import dev.rodriguez.managers.TokenManager;
import dev.rodriguez.managers.TokenManagerImpl;
import dev.rodriguez.model.User;
import dev.rodriguez.services.BookItemService;
import dev.rodriguez.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private final UserService service = new UserService();

    private TokenManager tokenManager = new TokenManagerImpl();


    public void authenticateLogin(Context ctx) {
        /**
         * Content-type: application/x-www-form-urlencoded
         */

        String userString = ctx.formParam("userName");
        String pwString = ctx.formParam("password");

        /**
         *
         *
         *

         version 0[works]:

         logger.info("{} attempted login", userString);
         if (userString != null && userString.equals("username")) {
         if (pwString != null && pwString.equals("password")) {
         logger.info("Successful login!");
         //send back token
         ctx.header("Authorization", "admin-auth-token");
         ctx.status(200);
         return;
         }
         throw new UnauthorizedResponse("Error: Incorrect Password");
         }
         */


        /**
         * Invoke service method, checking for user
         */

        if (service.doesUserExist(userString)) {
            User user = service.getUserByUserName(userString);
            if (pwString.equals(user.getPassword()) && pwString != null) {
                logger.info("Successful login!");
                //send back token
                ctx.header("Authorization", "admin-auth-token");
                ctx.status(200);
                return;
            }
            throw new UnauthorizedResponse("Error: Incorrect Password");
        }
        throw new UnauthorizedResponse("Error: Unrecognized user");


        /**
         * if (service.doesUserExist(userString, pwString)) {
         *             User user = service.getUserByUserName(userString);
         *             if (pwString.equals(user.getPassword()) && pwString != null) {
         *                 logger.info("Successful login!");
         *                 //send back token
         *                 ctx.header("Authorization", "admin-auth-token");
         *                 ctx.status(200);
         *                 return;
         *             }
         *             throw new UnauthorizedResponse("Error: Incorrect Password");
         *         }
         *         throw new UnauthorizedResponse("Error: Unrecognized user");
         *
         */

    }



    public void authorizeToken(Context ctx) {
        logger.info("attempting to authorize token...");

        /**
         *
         */
        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String authHeader = ctx.header("Authorization");

        /**
         * version 0 [works]:
         *

        if(authHeader!=null && authHeader.equals("admin-auth-token")) {
            logger.info("Request authorized. Proceeding to handler...");
            ctx.status(200);
        } else {
            logger.warn("Error: Improper authorization.");
            ctx.status(401);
            throw new UnauthorizedResponse("Error: Improper authorization.");
        }

        */

        if(authHeader!=null && authHeader.equals("admin-auth-token")) {
            logger.info("Request authorized. Proceeding to handler...");
            ctx.status(200);
        } else {
            logger.warn("Error: Improper authorization.");
            ctx.status(401);
            throw new UnauthorizedResponse("Error: Improper authorization.");
        }


        /**
         * version 1 [works]:
         *

         if(authHeader!=null && tokenManager.authorizeToken(authHeader)) {
         logger.info("Request authorized. Proceeding to handler...");
         ctx.status(200);
         } else {
         logger.warn("Error: Improper authorization.");
         ctx.status(401);
         throw new UnauthorizedResponse("Error: Improper authorization.");
         }


         *
         */




    }

    public void userLogin(Context ctx) {
        User user = ctx.bodyAsClass(User.class);
        if(service.checkPassword(user)) {
            String newToken = tokenManager.issueToken(user.getUserName(),user.getPassword());
            ctx.header("Authorization", "admin-auth-token");
            ctx.status(200);

            //ctx.json(newToken);

            /**

             version 1[works]:

             String newToken = tokenManager.issueToken(user.getUserName(),user.getPassword());
            ctx.header("Authorization", newToken);
            ctx.status(200);
            ctx.json(newToken);

            */

        } else {
            ctx.status(401);
        }

    }



}

