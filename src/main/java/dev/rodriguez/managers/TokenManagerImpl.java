package dev.rodriguez.managers;

import dev.rodriguez.services.UserService;
import io.javalin.http.ForbiddenResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;

public class TokenManagerImpl implements TokenManager {

    private Logger logger = LoggerFactory.getLogger(TokenManagerImpl.class);

    private UserService service = new UserService();

    private final Key key;

    public TokenManagerImpl() {
        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    @Override
    public boolean authorizeToken(String token) {
        try {
            String clientUsername = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getAudience();
            String clientPassword = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
            if(service.checkPassword(clientUsername,clientPassword)){
                logger.info("Success: token authorized");
                return true;
            } else {
                logger.info("Error: token not authorized");
                return false;
            }

        } catch(Exception ex) {
            throw new ForbiddenResponse("Error[Token]:" + ex.getMessage());
        }
    }

    @Override
    public String issueToken(String username, String password) {
        String token = Jwts.builder().setAudience(username).setSubject(password).signWith(key).compact();
        logger.info("token issued");
        return token;
    }

}
