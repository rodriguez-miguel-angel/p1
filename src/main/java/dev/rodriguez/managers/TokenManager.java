package dev.rodriguez.managers;

public interface TokenManager {

    boolean authorizeToken(String token);

    String issueToken(String username, String password);

}
