-- init staff

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Adam', 'Lee', 'adam.lee@abc.net', 'a.lee', 'cat-lover');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Alicia', 'Kim', 'alicia.kim@abc.net', 'a.kim', 'dog-lover');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Alex', 'Garcia', 'alex.garcia@abc.net', 'a.garcia', 'parakeet-lover');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Bob', 'Smith', 'bob.smith@abc.net', 'b.smith', 'password01');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Beatriz', 'Soto', 'beatriz.soto@abc.net', 'b.soto', 'password02');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Cindy', 'Davis', 'cindy.davis@abc.net', 'c.davis', 'password01');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Don', 'Johnson', 'don.johnson@abc.net', 'd.johnson', 'abcdef');

insert into staff (first_name, last_name, email, user_name, user_password) 
values ('Ernesto', 'Mejia', 'ernesto.mejia@abc.net', 'e.mejia', '123456');


insert into staff (first_name, middle_name, last_name, email, user_name, user_password) 
values ('Jose', 'Luis', 'Hernandez', 'jose.hernandez@abc.net', 'j.hernandez', 'password');

insert into staff (first_name, middle_name, last_name, email, user_name, user_password) 
values ('Francisco', 'Javier', 'Jimenez', 'francisco.jimenez@abc.net', 'f.jimenez', 'password');
