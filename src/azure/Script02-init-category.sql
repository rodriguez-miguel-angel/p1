-- init category
insert into category (name) values ('FICTION');
insert into category (name) values ('NONFICTION');
insert into category (name) values ('HISTORY');
insert into category (name) values ('PHILOSOPHY');
insert into category (name) values ('POETRY');
insert into category (name) values ('BUSINESS');
insert into category (name) values ('SCIENCE');
insert into category (name) values ('ART');
