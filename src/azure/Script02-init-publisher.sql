-- init publisher
insert into publisher (name) values ('PENGUIN');
insert into publisher (name) values ('PENGUIN_CLASSICS');
insert into publisher (name) values ('MODERN_LIBRARY');
insert into publisher (name) values ('OXFORD_UNIVERSITY_PRESS');
insert into publisher (name) values ('HARVARD_PRESS');
insert into publisher (name) values ('STANFORD_PRESS');
insert into publisher (name) values ('CENGAGE');
insert into publisher (name) values ('PEARSON');